import 'package:flutter/material.dart';
import 'package:insta_farsi/my_home_page.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'instagramm',
      theme: new ThemeData(
        fontFamily: 'Nazanin',
        primaryColor: Colors.black,
          primaryIconTheme: IconThemeData(color: Colors.black),
          // ignore: deprecated_member_use
          primaryTextTheme: TextTheme(title: TextStyle(color: Colors.black))
      ),
      home: new Directionality(textDirection: TextDirection.rtl, child: MyHomePages())
    );
  }

}

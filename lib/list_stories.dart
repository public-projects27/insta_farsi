

import 'package:flutter/material.dart';



class ListStories extends StatelessWidget {


  final topText = new Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        new Text(
          "استوری ها",
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        new Row(
          children: <Widget>[
            new Icon(Icons.play_arrow),
            new Text(
                "مشاهده همه", style: TextStyle(fontWeight: FontWeight.bold)),

          ],
        )
      ]
  );

  final stories = new Expanded(
      child: new Padding(
          padding: EdgeInsets.only(top: 6.0),
          child: new ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: 10,
              itemBuilder: (context, index) {
                if (index == 0) {
                  return new Stack(
//                      alignment: Alignment.bottomRight,
                      children: <Widget>[
                        new Container(
                            width: 50.0,
                            height: 50.0,
                            margin: const EdgeInsets.symmetric(horizontal: 2.0),
                            decoration: new BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  //fit: BoxFit.cover,
                                    image: NetworkImage(
                                        "http://www.hotavatars.com/wp-content/uploads/2019/01/I80W1Q0.png" //MyAvatar
                                    )
                                )
                            )
                        ),
                        new Positioned(
                            right: 5.0,
                            bottom: 0.0,
                            child: new CircleAvatar(
                                radius: 9.0,
                                backgroundColor: Color(0xffffffff),
                                     child: CircleAvatar(
                                      backgroundColor: Colors.purpleAccent,
                                      radius: 8.0,
                                      child: new Icon(
                                          Icons.add,
                                          size: 11.0,
                                          color: Colors.white
                                )
                            )
                        )
                        )
                      ]
                  );
                }
                else {
                  return new Stack(
//                      alignment: Alignment.bottomRight,
                      children: <Widget>[
                        new Container(
                            width: 50.0,
                            height: 50.0,
                            margin: const EdgeInsets.symmetric(horizontal: 4.0),
                            decoration: new BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  //fit: BoxFit.cover,
                                    image: NetworkImage(
                                        "https://www.shareicon.net/data/2016/05/24/770139_man_512x512.png" //FriendsAvatar
                                    )
                                )
                            )
                        )
                      ]
                  );
                }
              }
          )
          )
      );

  @override
  Widget build(BuildContext context) {
    return new Container(
        margin: const EdgeInsets.all(12.0),
        child: new Column(


            children: <Widget>[
              topText,
              stories,
            ]
        )
    );
  }


}